function [densG densG_std densR densR_std] = downstreamAnalysis_applied(data_filepath, validpath, colors, proteins, saveData)

if nargin
    registration_filepath = data_filepath;
else
    registration_filepath = 'y:\DOL Calibration\Data\felix\analysis\felix_rg-A2C-0.2_b-80-percentile.mat';
    data_filepath = registration_filepath;
    saveData = 0;
    colors = {'Red', 'Green', 'Blue'};
    proteins = {'SNAP-tag', 'HaloTag', 'GFP'};        % Order according to colors
    validpath = 'y:\DOL Calibration\Data\sigi\analysis\sigi_base_cherryPicking.mat'; 
end

load(registration_filepath);
load(validpath);
% valid = cherryPick.valid;

valid = true(length(replicate),1);    %% uncomment to ignore cherryPick

registration_filepath = data_filepath;
[pathstr,name,~] = fileparts(registration_filepath);
if saveData
    mkdir(pathstr,name);
    savefolder = fullfile(pathstr,name);
end
exp_system = name;

%% Global Parameters

Cells={'gSEP' 'LynG'};
inctime=[0.25 0.5 1 3 16];
concrange=[0 0.1 1 5 10 50 100 250];

sampleName = cell(2,5,8);
for l=1:2
for t=1:5
for c=1:8
sampleName{l,t,c} = [Cells{l} ' ' num2str(inctime(t)) 'h ' num2str(concrange(c)) 'nM'];
end
end
end

conditions = cell(length(CellType),1);
for i=1:length(CellType)
conditions{i} = [CellType{i} ' ' num2str(incubation_time(i)) 'h ' num2str(concentration(i)) 'nM'];
conditions{i} = strrep(conditions{i},'16h','overnight');
end

clr_c = parula(8);
clr_t = lines(5);

%% Colocalisation Threshold

%Select all replicates where the registration worked in both channels
Selection = (strcmp(FlagRed,'Registration successfull') |...
    strcmp(FlagRed,'successfull registration')) &...
    (strcmp(FlagGreen,'Registration successfull') |...
    strcmp(FlagGreen,'successfull registration'));

SelectionMatrix = Selection;
for i=1:39
    SelectionMatrix=[SelectionMatrix Selection];
end

ColGreen = ColocalizationBlueGreen(SelectionMatrix);
ColGreen = reshape(ColGreen,[length(ColGreen)/40,40]);
ColGreenRandom = ColocalizationGreenRandom(SelectionMatrix);
ColGreenRandom = reshape(ColGreenRandom,[length(ColGreenRandom)/40,40]);
ColRed = ColocalizationBlueRed(SelectionMatrix);
ColRed = reshape(ColRed,[length(ColRed)/40,40]);
ColRedRandom = ColocalizationRedRandom(SelectionMatrix);
ColRedRandom = reshape(ColRedRandom,[length(ColRedRandom)/40,40]);

% Find Tolerance Threshold as mean value
meanGreen = mean(ColGreen,1);
meanGreenRandom = mean(ColGreenRandom,1);
meanRed = mean(ColRed,1);
meanRedRandom = mean(ColRedRandom,1);

[~,indexGreen] = max(meanGreen/max(meanGreen)-meanGreenRandom/max(meanGreenRandom));
ToleranceGreen = round(indexGreen)/10;
FinalThresholdGreen = ToleranceGreen;

[~,indexRed] = max(meanRed/max(meanRed)-meanRedRandom/max(meanRedRandom));
ToleranceRed = round(indexRed)/10;
FinalThresholdRed = ToleranceRed; %1.4;


%% Create 5x8 DOL-Result and number of particle-matrices
%%Create matrix of mean values of labeling efficiencies(x-Axis: concentration 0 to 250nM, y-Axis:
%%incubation time 0.25 to 16 h, 5x8 matrix

pGreen = pGreen(:,round(FinalThresholdGreen*10));
pBlue = pBlue(:,round(FinalThresholdGreen*10));
pGreenRandom = pGreenRandom(:,round(FinalThresholdGreen*10));
pRed = pRed(:,round(FinalThresholdRed*10));
pRedRandom = pRedRandom(:,round(FinalThresholdRed*10));

%% Calculate Particle Densities
% Pixel size in �m
pixelSize = 0.104;

Density_Blue = BlueParticles./(AllAreas*(pixelSize^2));
Density_Green = GreenParticles./(AllAreas*(pixelSize^2));
Density_Red = RedParticles./(AllAreas*(pixelSize^2));

if strcmp(exp_system, 'felix')
    pixelSize_2 = 0.095;
    index = incubation_time == 16 | incubation_time == 3 | incubation_time == 1;
    
    Density_Blue(index) = BlueParticles(index)./(AllAreas(index)*(pixelSize_2^2));
    Density_Green(index) = GreenParticles(index)./(AllAreas(index)*(pixelSize_2^2));
    Density_Red(index) = RedParticles(index)./(AllAreas(index)*(pixelSize_2^2));
end

%%Correct DOL for particle density
pGreen = pGreen./(-0.17*Density_Green+1);
pRed = pRed./(-0.17*Density_Red+1);
pBlue = pBlue./(-0.17*Density_Blue+1);

%Normalize Background to single emitter intensity
if exist(SingleEmitter_Blue)
    BackgroundBlue = BackgroundBlue./mean(SingleEmitter_Blue);
    BackgroundGreen = BackgroundGreen./mean(SingleEmitter_Green);
    BackgroundRed = BackgroundRed./mean(SingleEmitter_Red);
end

%% Parse Data for each condition

[MeanDOLGreen,...
StdDOLGreen,...
MeanDOLRed,...
StdDOLRed,...
MeanDOLBlue,...
StdDOLBlue,...
MeanDOLGreenRandom,...
MeanDOLRedRandom,...
MeanParticlesGreen,...
StdParticlesGreen,...
MeanParticlesRed,...
StdParticlesRed,...
MeanParticlesBlue,...
StdParticlesBlue,...
BackgroundGreen_all,...
StdBackgroundGreen_all,...
BackgroundBlue_all,...
StdBackgroundBlue_all,...
BackgroundRed_all,...
StdBackgroundRed_all,...
numCells]...
    = deal(zeros(2,5,8));

% BackgroundGreenall (and others) are obsolete with all backgrounds in
% BackgroundBlue

sigDOL_Halo_SNAP=zeros(5,8);

for l=1:2
    for c=1:8
        for t=1:5
            [~,sigDOL_Halo_SNAP(t,c)]=ranksum(pGreen(concentration== concrange(c) & incubation_time==inctime(t) & strcmp (CellType,'gSEP')),pRed(concentration== concrange(c) & incubation_time==inctime(t) & strcmp (CellType,'gSEP')));


            %DOL
            MeanDOLGreen(l,t,c) = mean(pGreen...
                (valid & ...
                isfinite(pGreen) &...
                concentration == concrange(c) & ...
                incubation_time == inctime(t) & ...
                strcmp (CellType,Cells{l})) );
            StdDOLGreen(l,t,c) = std(pGreen...
                (valid & ...
                isfinite(pGreen) &...
                concentration == concrange(c) & ...
                incubation_time == inctime(t) & ...
                strcmp (CellType,Cells{l})) );

            MeanDOLRed(l,t,c) = mean(pRed...
                (valid & ...
                isfinite(pRed) &...
                concentration == concrange(c) & ...
                incubation_time == inctime(t) & ...
                strcmp (CellType,Cells{l})) );
            StdDOLRed(l,t,c) = std(pRed...
                (valid & ...
                isfinite(pRed) &...
                concentration == concrange(c) & ...
                incubation_time == inctime(t) & ...
                strcmp (CellType,Cells{l})) );

            MeanDOLBlue(l,t,c) = mean(pBlue...
                (valid & ...
                isfinite(pBlue) &...
                concentration == concrange(c) & incubation_time == inctime(t) & strcmp (CellType,Cells{l})) );
            StdDOLBlue(l,t,c) = std(pBlue...
                (valid & ...
                isfinite(pBlue) &...
                concentration == concrange(c) & incubation_time == inctime(t) & strcmp (CellType,Cells{l})) );

            MeanDOLGreenRandom(l,t,c) = mean(pGreenRandom...
                (valid & ...
                isfinite(pGreenRandom) &...
                concentration == concrange(c) & incubation_time == inctime(t) & strcmp(CellType,Cells{l})) );
            MeanDOLRedRandom(l,t,c) = mean(pRedRandom...
                (valid & ...
                isfinite(pRedRandom) &...
                concentration == concrange(c) & incubation_time == inctime(t) & strcmp(CellType,Cells{l})) );

            %particle densities
            MeanParticlesGreen(l,t,c) = mean(Density_Green...
                (valid & ...
                isfinite(Density_Green) &...
                concentration == concrange(c) & incubation_time == inctime(t) & strcmp (CellType,Cells{l})) );
            StdParticlesGreen(l,t,c) = std(Density_Green...
                (valid & ...
                isfinite(Density_Green) &...
                concentration == concrange(c) & incubation_time == inctime(t) & strcmp (CellType,Cells{l})) );

            MeanParticlesRed(l,t,c) = mean(Density_Red...
                (valid & ...
                isfinite(Density_Red) &...
                concentration == concrange(c) & incubation_time == inctime(t) & strcmp (CellType,Cells{l})) );
            StdParticlesRed(l,t,c) = std(Density_Red...
                (valid & ...
                isfinite(Density_Red) &...
                concentration == concrange(c) & incubation_time == inctime(t) & strcmp (CellType,Cells{l})) );

            MeanParticlesBlue(l,t,c) = mean(Density_Blue...
                (valid & ...
                isfinite(Density_Blue) &...
                concentration == concrange(c) & incubation_time == inctime(t) & strcmp (CellType,Cells{l})) );
            StdParticlesBlue(l,t,c) = std(Density_Blue...
                (valid & ...
                isfinite(Density_Blue) &...
                concentration == concrange(c) & incubation_time == inctime(t) & strcmp (CellType,Cells{l})) );
            
            %Background
            BackgroundGreen_all(l,t,c) = mean(BackgroundGreen...
                (valid & ...
                concentration == concrange(c) & incubation_time == inctime(t) & strcmp (CellType,Cells{l})) );
            StdBackgroundGreen_all(l,t,c) = std(BackgroundGreen...
                (valid & ...
                concentration == concrange(c) & incubation_time == inctime(t) & strcmp (CellType,Cells{l})) );

            BackgroundRed_all(l,t,c) = mean(BackgroundRed...
                (valid & ...
                concentration == concrange(c) & incubation_time == inctime(t) & strcmp (CellType,Cells{l})) );
            StdBackgroundRed_all(l,t,c) = std(BackgroundRed...
                (valid & ...
                concentration == concrange(c) & incubation_time == inctime(t) & strcmp (CellType,Cells{l})) );

            BackgroundBlue_all(l,t,c) = mean(BackgroundBlue...
                (valid & ...
                concentration == concrange(c) & incubation_time == inctime(t) & strcmp (CellType,Cells{l})) );
            StdBackgroundBlue_all(l,t,c) = std(BackgroundBlue...
                (valid & ...
                concentration == concrange(c) & incubation_time == inctime(t) & strcmp (CellType,Cells{l})) );

            
            %numCells
            numCells(l,t,c) = sum(...
                valid & concentration == concrange(c) & incubation_time == inctime(t) & strcmp (CellType,Cells{l}));

            
        end

    end

end

%%

densG = MeanParticlesGreen(2,2,:);
densG_std = StdParticlesGreen(2,2,:);
densR = MeanParticlesRed(2,2,:);
densR_std = StdParticlesRed(2,2,:);

end