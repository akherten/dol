function [time, celltype, concentration, replicate, channel, fittype, threshold] = conditionsFromPathTS(file)

[filepath, name, ~] = fileparts(file);

% identify cell type
if strfind(name,'gSEP')
    celltype = 'gSEP';
elseif strfind(name,'LynG')
    celltype = 'LynG';
elseif strfind(name, 'fullImage')
    celltype = 'Sims';
else
    celltype = [];
end

% incubation time
incubation_time_temp=filepath;
if strfind(incubation_time_temp, 'overnight')
    incubation_time_temp=16;
elseif strfind(incubation_time_temp, '15min')
    incubation_time_temp=0.25;
elseif strfind(incubation_time_temp, '30min')
    incubation_time_temp=0.5;
elseif strfind(incubation_time_temp, '60min')
    incubation_time_temp=1;
elseif strfind(incubation_time_temp, '3h')
    incubation_time_temp=3;
else
    incubation_time_temp=[];
end

time=incubation_time_temp;

% concentration // density --> For density simulation data concentration == density
if strfind(celltype,'Sims')
    dashindex = strfind(name, '-');
    if ~isempty(dashindex)
        conc = strrep(name(dashindex-1:dashindex+1),'-','.');
        % replicate number for gSEP/LynG data
        replicate = str2double(name(dashindex+3:dashindex+4));
    else
        conc= [];
    end
else
    underscoreIndex = strfind(name, '_');
    nMIndex = strfind(name, 'nM');
    if isempty(nMIndex)
        conc = [];
        replicate = [];
    else
        Cstart = max(underscoreIndex(underscoreIndex<nMIndex)) + 1;
        Cend = nMIndex - 1;
        conc = name(Cstart:Cend);
        if strfind(conc, ',')
            conc = strrep(conc, ',', '.');
        end
        % replicate number for gSEP/LynG data
        replicate = str2double(name(nMIndex+3:nMIndex+4));
    end
end
concentration = str2double(conc);

% fittype
if strfind(filepath,'multiemitter')
    fittype = 'multi';
elseif strfind(filepath,'singleemitter')
    fittype = 'single';
else
    if strfind(name,'multiemitter')
        fittype = 'multi';
    elseif strfind(name,'singleemitter')
        fittype = 'single';
    else
        fittype = [];
    end
end

% channel
if strfind(name,'blue')
    channel = 'blue';
elseif strfind(name,'greenBleach')
    channel = 'greenbleach';
elseif strfind(name,'green')
    channel = 'green';
elseif strfind(name,'red')
    channel = 'red';
else
    channel = [];
end

% threshold
thresIndex = strfind(name,'thres');
if isempty(thresIndex)
    threshold = [];
else
    threshold = name(thresIndex+6:end);
end

end