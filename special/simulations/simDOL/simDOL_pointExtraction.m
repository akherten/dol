% Script to collect point coordinates, identifiers and properties for the
% simulated cell data.

% channels / samples:
% - ref
%   points from u-track on samples with DOL = 1, named blue in u-track
%   (channel 1)
% - sub
%   points from u-track on DOL < 1 subsets, named green in u-track (channel
%   2)
% - GT
%   ground truth positions from simulation data

movieListPath = 'y:\DOL Calibration\Data\SimData_realistic\simultedDOL\u-track\movieList.mat';
savePath = '';
GTcoords = 'e:\DataSync\Doktorarbeit\00_Science\Calibration\DataSimulation\testSTORM\DOLSim-RealBack\PointsRaw';

movielist = load(movieListPath);
MDpaths = movielist.ML.movieDataFile_;

% assign cell properties derived from filename
[GroundTruthDensity, GroundTruthDOL, replicate] = simDOL_conditionsFromString(MDpaths);

% collect points from u-track movieList
[PointDetectionParameters, Points_ref_x, Points_ref_y, Points_ref_A,...
    Points_ref_c, Points_ref_s] = pointsFromMovieData(MDpaths, 1);   
[~, Points_sub_x, Points_sub_y, Points_sub_A,...
    Points_sub_c, Points_sub_s] = pointsFromMovieData(MDpaths, 2); 

% collect points from ground truth
% 
GT = load('y:\DOL Calibration\Data\SimData_realistic\simultedDOL\groundTruthDOL1.mat', ...
    'x', 'y', 'replicate', 'GroundTruthDensity');
for i = 1:length(replicate)
    Points_GT_x{i} = GT.x{replicate(i) == GT.replicate & GroundTruthDensity(i) == GT.GroundTruthDensity};
    Points_GT_y{i} = GT.y{replicate(i) == GT.replicate & GroundTruthDensity(i) == GT.GroundTruthDensity};
end
clear GT

% calculate cell areas from mask files
AllAreas = simulations_AreaFromMask(MDpaths);