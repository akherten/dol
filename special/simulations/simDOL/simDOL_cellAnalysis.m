% Filtering amplitud threshold
filterPercentile = [];  % set to [] to skip filtering
filterThreshold = 511; % set to [] to skip filtering

% Colocalisation tolerance    
tolerance = (0.1:0.1:4);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Filter Points %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


if ~isempty(filterThreshold)
    
    % Keep unfiltered point amplitudes
    Points_ref_A_unfiltered = Points_ref_A;
    Points_sub_A_unfiltered = Points_sub_A;
    
    [Points_ref_A, Points_ref_x, Points_ref_y, Points_ref_c, Points_ref_s] = ...
        filterPointsByThreshold(filterThreshold, ...
        Points_ref_A, Points_ref_x, Points_ref_y, Points_ref_c, Points_ref_s);
    
    [Points_sub_A, Points_sub_x, Points_sub_y, Points_sub_c, Points_sub_s] = ...
        filterPointsByThreshold(filterThreshold, ...
        Points_sub_A, Points_sub_x, Points_sub_y, Points_sub_c, Points_sub_s);

end


% Rotate Points %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Preallocate Variables
[Points_refRot_x, Points_refRot_y, Points_subRot_x, Points_subRot_y] = ...
    deal(cell(size(Points_ref_x)));

for i = 1:length(Points_ref_x)
    Points_refRot_x{i} = Points_ref_y{i};
    Points_refRot_y{i} = 512 - Points_ref_x{i};
    Points_subRot_x{i} = Points_sub_y{i};
    Points_subRot_y{i} = 512 - Points_sub_x{i};
end


% Colocalisation %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('Colocalisation analysis')

% Preallocate Variables
[Particles_ref, Particles_sub, Particles_GT]...
    = deal(zeros(length(Points_ref_x),1));

[Col_refsub, multi_refsub, pSub2Ref, ...
    Col_refsubRot, multi_refsubRot, pSubRot2Ref,...
    Col_GTsub, multi_GTsub, pSub2GT,...
    Col_GTsubRot, multi_GTsubRot, pSubRot2GT,...
    Col_GTref, multi_GTref, pRef2GT,...
    Col_GTrefRot, multi_GTrefRot, pRefRot2GT]...
    = deal(zeros(length(Points_ref_x),length(tolerance)));

for i = 1:length(Points_ref_x)
    
    for t = 1:length(tolerance)

        dispProgress(i, length(Points_ref_x), t, length(tolerance))

        
        % sub to ref
        [Particles_ref(i), Particles_sub(i), Col_refsub(i,t), multi_refsub(i,t)] = ...
            detectColocalisation(Points_ref_x{i}, Points_ref_y{i}, ...
            Points_sub_x{i}, Points_sub_y{i}, ...
            tolerance(t), tolerance(t));
        
        % rotated sub to ref
        [~, ~, Col_refsubRot(i,t), multi_refsubRot(i,t)] = ...
            detectColocalisation(Points_ref_x{i}, Points_ref_y{i}, ...
            Points_subRot_x{i}, Points_subRot_y{i}, ...
            tolerance(t), tolerance(t));
        
        % sub to ground truth
        [Particles_GT(i), ~, Col_GTsub(i,t), multi_GTsub(i,t)] = ...
            detectColocalisation(Points_GT_x{i}, Points_GT_y{i}, ...
            Points_sub_x{i}, Points_sub_y{i}, ...
            tolerance(t), tolerance(t));
        
        % rotated sub to GT
        [~, ~, Col_GTsubRot(i,t), multi_GTsubRot(i,t)] = ...
            detectColocalisation(Points_GT_x{i}, Points_GT_y{i}, ...
            Points_subRot_x{i}, Points_subRot_y{i}, ...
            tolerance(t), tolerance(t));
        
        % ref to ground truth (= recall)
        [~, ~, Col_GTref(i,t), multi_GTref(i,t)] = ...
            detectColocalisation(Points_GT_x{i}, Points_GT_y{i}, ...
            Points_ref_x{i}, Points_ref_y{i}, ...
            tolerance(t), tolerance(t));
        
        % rotated ref to ground truth (= recall random)
        [~, ~, Col_GTrefRot(i,t), multi_GTrefRot(i,t)] = ...
            detectColocalisation(Points_GT_x{i}, Points_GT_y{i}, ...
            Points_refRot_x{i}, Points_refRot_y{i}, ...
            tolerance(t), tolerance(t));
        
        % recall in "blue channel" from ground truth
        pRef2GT(i,t) = Col_GTref(i,t) / Particles_GT(i);
        pRefRot2GT(i,t) = Col_GTrefRot(i,t) / Particles_GT(i);
        
        % recall in "green channel" from ground truth in blue channel
        pSub2GT(i,t) = Col_GTsub(i,t) / Particles_GT(i);
        pSubRot2GT(i,t) = Col_GTsubRot(i,t) / Particles_GT(i);
        
        % DOL green/blue normalized to blue channel
        pSub2Ref(i,t) = Col_refsub(i,t) / Particles_ref(i);
        pSubRot2Ref(i,t) = Col_refsubRot(i,t) / Particles_ref(i);
        
    end
    
end