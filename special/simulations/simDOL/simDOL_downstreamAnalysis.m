
valid = true(length(replicate),1);

% Calculate Densities
pixelSize = 0.104; % in �m
Density_ref = Particles_ref ./ (AllAreas * pixelSize^2);
Density_sub = Particles_sub ./ (AllAreas * pixelSize^2);
Density_GT = Particles_GT ./ (AllAreas * pixelSize^2);

% DOL
finalThreshold_refsub = colocalisationThreshold(Col_refsub, Col_refsubRot, tolerance, 'plot');
finalThreshold_GTsub = colocalisationThreshold(Col_GTsub, Col_GTsubRot, tolerance, 'plot');
finalThreshold_GTref = colocalisationThreshold(Col_GTref, Col_GTrefRot, tolerance, 'plot');
foundDOL_ref = pSub2Ref(:,tolerance == finalThreshold_refsub);
foundDOL_GT = pSub2GT(:,tolerance == finalThreshold_GTsub);
foundRecall = pRef2GT(:, tolerance == finalThreshold_GTref);

% Denisty correction (from u-track single found density vs. recall linfit)
DOL_Corr = foundDOL_GT ./ (-0.2836 * Density_sub + 0.7025);

% Mean values and stddev
[groups, densityID, dolID] = findgroups(GroundTruthDensity, GroundTruthDOL);

ConditionValues = meanAndStd(valid, groups, ...
        foundDOL_ref, foundDOL_GT, DOL_Corr);
ConditionValues.numCells = splitapply(@length,groups,groups);
ConditionValues.ID = struct('GroundTruthDensity', densityID, 'GroundTruthDOL', dolID);