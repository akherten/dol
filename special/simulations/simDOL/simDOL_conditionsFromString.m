 % function for extraction identifiers for simulation data from a string.
% Input
% single string or cell array of strings, where the string contains the
% ground truth density and the replicate.
% 
% all identifiers may only occur once in the string.

function [GroundTruthDensity, GroundTruthDOL, replicate] = simulations_conditionsFromString(input)

    if iscellstr(input)
        for i = 1:length(input)
            myString = input{i};
            [GroundTruthDensity(i), GroundTruthDOL(i), replicate(i)]...
                = stringSearch(myString);
        end
    elseif ischar(input)
        [GroundTruthDensity, GroundTruthDOL, replicate]...
            = stringSearch(input);
    else
        error('Input Error. Must be string or cell array of strings')
    end
    
    GroundTruthDensity = GroundTruthDensity';
    GroundTruthDOL = GroundTruthDOL';
    
end

function [GroundTruthDensity, GroundTruthDOL, replicate] = stringSearch(input)
        
    GroundTruthDensity = str2num(strrep(input(strfind(input,'density_')+8:strfind(input,'density_')+10),'-','.'));
    GroundTruthDOL = str2num(strrep(input(strfind(input,'DOL_')+4:strfind(input,'DOL_')+7),'-','.'));
    underIndex = strfind(input,'_');
    replicate = str2num(input(underIndex(end)-2:underIndex(end)-1));
    
end