% split data in high and low density
CV_high = ConditionValues.ID.GroundTruthDensity == 1.6;
CV_low = ~CV_high;
% mean found DOL vs. simulated DOL
figure
errorbar(ConditionValues.ID.GroundTruthDOL(CV_high), ConditionValues.Mean_foundDOL_GT(CV_high), ConditionValues.Std_foundDOL_GT(CV_high), 'LineStyle', 'none', 'Marker', 'x')
hold on
errorbar(ConditionValues.ID.GroundTruthDOL(CV_low), ConditionValues.Mean_foundDOL_GT(CV_low), ConditionValues.Std_foundDOL_GT(CV_low), 'LineStyle', 'none', 'Marker', 'x')
errorbar(ConditionValues.ID.GroundTruthDOL(CV_high), ConditionValues.Mean_DOL_Corr(CV_high), ConditionValues.Std_DOL_Corr(CV_high), 'LineStyle', 'none', 'Marker', 'x')
errorbar(ConditionValues.ID.GroundTruthDOL(CV_low), ConditionValues.Mean_DOL_Corr(CV_low), ConditionValues.Std_DOL_Corr(CV_low), 'LineStyle', 'none', 'Marker', 'x')
legend('uncorrected 1.6 �m^-^2', 'uncorrected 0.6 �m^-^2', 'corrected 1.6 �m^-^2', 'corrected 0.6 �m^-^2', 'location', 'northwest')
plot(xlim, ylim, '--k')
xlabel('simulated DOL')
ylabel('determined DOL')

% split data in high and low density
high = GroundTruthDensity == 1.6;
low = ~high;
% density correction: recall vs. found density
figure
scatter(Density_sub(low), (foundDOL_GT(low)./GroundTruthDOL(low)));
hold on
scatter(Density_sub(high), (foundDOL_GT(high)./GroundTruthDOL(high)));
scatter(Density_sub(Particles_sub < 100), (foundDOL_GT(Particles_sub < 100)./GroundTruthDOL(Particles_sub < 100)), 'filled')
linfit = fit(Density_sub, (foundDOL_GT./GroundTruthDOL), 'poly1');
plot(linfit, 'g');
legend('0.6 �m^-^2', '1.6 �m^-^2', '<100 points/cell',  'fit all')
text(.1, .1, sprintf('Recall = %.4f * x + %.4f', linfit.p1, linfit.p2), 'Color', 'g')
ylim([0 1]);
xlabel('found density')
ylabel('found DOL / simulated DOL')