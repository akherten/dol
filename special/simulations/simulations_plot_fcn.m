matFilePaths = {
    'y:\DOL Calibration\Data\SimData_realistic\green_background\Density-Simulation\analysis\thunderSTORM_single_t-2-0_fltr-0-0.7-2.7.mat'
    'y:\DOL Calibration\Data\SimData_realistic\green_background\Density-Simulation\analysis\thunderSTORM_multi_t-2-0_fltr-0-0.7-2.7.mat'
    'y:\DOL Calibration\Data\SimData_realistic\green_background\Density-Simulation\analysis\u-track_green_single_fltr-90-0.7-2.7.mat'
    'y:\DOL Calibration\Data\SimData_realistic\green_background\Density-Simulation\analysis\u-track_green_multi_fltr-90-0.7-2.7.mat'
    'y:\DOL Calibration\Data\SimData_realistic\green_background\Density-Simulation\analysis\thunderSTORM_single_t-2-0.mat'
    'y:\DOL Calibration\Data\SimData_realistic\green_background\Density-Simulation\analysis\thunderSTORM_multi_t-2-0.mat'
    'y:\DOL Calibration\Data\SimData_realistic\green_background\Density-Simulation\analysis\thunderSTORM_single_t-2-0_fltr-50-0.7-2.7.mat'
    'y:\DOL Calibration\Data\SimData_realistic\green_background\Density-Simulation\analysis\thunderSTORM_multi_t-2-0_fltr-50-0.7-2.7.mat'
    'y:\DOL Calibration\Data\SimData_realistic\green_background\Density-Simulation\analysis\thunderSTORM_single_t-2-0_fltr-90-0.7-2.7.mat'
    'y:\DOL Calibration\Data\SimData_realistic\green_background\Density-Simulation\analysis\thunderSTORM_multi_t-2-0_fltr-90-0.7-2.7.mat'
};

varNames = {
    'ts1f0'
    'tsxf0'
    'uT1f'
    'uTxf'
    'ts1'
    'tsx'
    'ts1f50'
    'tsxf50'
    'ts1f90'
    'tsxf90'
};

for i = 1:length(matFilePaths)
    temp = load(matFilePaths{i}, 'data');
    eval([varNames{i} ' = temp.data;']);
    eval([varNames{i} '.matFilePath = matFilePaths{i};']);
end

densities = uT1f.ID.GroundTruthDensity;

plotRecall(densities, uTxf, tsx, tsxf0, tsxf50, tsxf90);
plotFalsePositive(densities, uTxf, tsx, tsxf0, tsxf50, tsxf90);
% plotDensityCorrelation(densities, tsxf, tsx);
% plotDensityCorrection(tsxf);
% plotDensityCorrection(tsx);


function plotRecall(densities, varargin)

    % Recall
    % Errorbar plot of Recall in dependence of simulated point density
    figure
    axes
    hold on
    for i = 1:length(varargin)
        errorbar(densities, varargin{i}.Mean_Recall, varargin{i}.Std_Recall, ...
            'x-','MarkerSize',10, 'DisplayName', inputname(i+1));
    end
    legend('show')
    xlabel('Ground Truth Density [um^-^2]')
    ylabel('Recall')
    xlim([0 2.7])
    ylim([0 1])

end

function plotFalsePositive(densities, varargin)

    % False Positives
    % Errorbar plot of false positive density in dependence of simulated point
    % density
    figure
    axes
    hold on
    for i = 1:length(varargin)
        errorbar(densities, varargin{i}.Mean_DensityFalsePos, varargin{i}.Std_DensityFalsePos, ...
            'x-','MarkerSize',10, 'DisplayName', inputname(i+1));
    end

    legend('show')
    xlabel('Ground Truth Density [um^-^2]')
    ylabel('False Positive Density [um^-^2]')
    xlim([-.1 2.7])
    ylim([0 0.2])

end

function plotDensityCorrelation(densities, varargin)

    % plot found vs. simulated density
    figure
    axes
    hold on
    for i = 1:length(varargin)
        errorbar(densities, varargin{i}.Mean_DensityBlue, varargin{i}.Std_DensityBlue, ...
            'x-','MarkerSize',10, 'DisplayName', inputname(i+1));
    end

    legend('show')
    
    xlim([-0.1 2.1])
    ylim(xlim)
    plot(xlim, ylim, '--k')

    xlabel('simulated density [um^-^2]')
    ylabel('found density [um^-^2]')

end

function plotDensityCorrection(data)

    % Density Correction on u-track single emitter data
    load(data.matFilePath, 'DensityBlue', 'Recall', 'GroundTruthDensity');
    % fit doesn't accept recall = NaN for density = 0
    density = DensityBlue(~isnan(Recall));
    recall = Recall(~isnan(Recall));
    linfit = fit(density, recall, 'poly1');
%     conf = confint(linfit);
    figure
    scatter(DensityBlue, Recall)
    hold on
    plot(linfit)
    text(.1, .1, sprintf('Recall = %.4f * x + %.4f', linfit.p1, linfit.p2), 'Color', 'red')

    p11 = predint(linfit,xlim,0.95);
    plot(xlim,p11, 'r:');

    legend({inputname(1) 'linear fit' '95 % prediction'})
    xlabel('Found density [um^-^2]')
    ylabel('Recall')
    ylim([0 1])

end