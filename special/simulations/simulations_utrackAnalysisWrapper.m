
movieLists = {
    % green background, multi emitter
    'y:\DOL Calibration\Data\SimData_realistic\green_background\Density-Simulation\u-track multi\movieList_Density-Simulation_green_multi.mat'
    % green background, single emitter
    'y:\DOL Calibration\Data\SimData_realistic\green_background\Density-Simulation\u-track single\movieList_Density-Simulation_green_single.mat'
    % red background, multi emitter
    'y:\DOL Calibration\Data\SimData_realistic\red_background\Density-Simulation\u-track multi\movieList_Density-Simulation_red_multi.mat'
    % red background, single emitter
    'y:\DOL Calibration\Data\SimData_realistic\red_background\Density-Simulation\u-track single\movieList_Density-Simulation_red_single.mat'};
saves = {
    % green background, multi emitter
    'y:\DOL Calibration\Data\SimData_realistic\green_background\Density-Simulation\analysis\u-track_green_multi.mat'
    % green background, single emitter
    'y:\DOL Calibration\Data\SimData_realistic\green_background\Density-Simulation\analysis\u-track_green_single.mat'
    % red background, multi emitter
    'y:\DOL Calibration\Data\SimData_realistic\red_background\Density-Simulation\analysis\u-track_red_multi.mat'
    % red background, single emitter
    'y:\DOL Calibration\Data\SimData_realistic\red_background\Density-Simulation\analysis\u-track_red_single.mat'};



for ml = 1:length(movieLists)
    
    analyse(movieLists{ml}, saves{ml});

end
    
function analyse(movieListPath, savePath)
    
    coords_root = 'y:\DOL Calibration\Data\SimData_realistic\points_raw_coords\';

    movielist = load(movieListPath);
    MDpaths = movielist.ML.movieDataFile_;

    % assign cell properties derived from filename
    [GroundTruthDensity, replicate] = simulations_conditionsFromString(MDpaths);

    % collect points from u-track movieList
    [PointDetectionParameters, Points_Blue_x, Points_Blue_y, Points_Blue_A,...
        Points_Blue_c, Points_Blue_s] = pointsFromMovieData(MDpaths, 1);    

    % collect points from ground truth
    [Points_Green_x, Points_Green_y] = ...
        simulations_pointsFromGroundTruth(GroundTruthDensity, replicate, coords_root, MDpaths);

    % calculate cell areas from mask files
    AllAreas = simulations_AreaFromMask(MDpaths);
    
    simulations_cellAnalysis
    
    simulations_downstreamAnalysis
    
    save(savePath);
    
end
    
    