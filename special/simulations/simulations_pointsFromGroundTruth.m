function [x,y] = simulations_pointsFromGroundTruth(density, replicate, coords_root, movieDataPath)


    if iscellstr(movieDataPath)
        [x, y] = deal(cell(size(movieDataPath)));
        for i = 1:length(movieDataPath)
            dispProgress(i, length(movieDataPath))
            [x{i},y{i}] = getCoords(density(i), replicate(i), coords_root, movieDataPath{i});
        end
    elseif ischar(movieDataPath)
        [x,y] = getCoords(density, replicate, coords_root, movieDataPath);
    else
        error('Input Error. Must be string or cell array of strings')
    end
    
end

function [x,y] = getCoords(density, replicate, coords_root, movieDataPath)
    
    if density > 0
        dirlist = dir(coords_root);
        density_str = strrep(num2str(density,'%.1f'),'.','-');
        index = ~cellfun(@isempty,strfind({dirlist.name}, density_str));

        subdir = fullfile(coords_root,dirlist(index).name);

        replicate_str = num2str(replicate, '%02d');
        subdirlist = dir(subdir);
        index2 = ~cellfun(@isempty,strfind({subdirlist.name}, replicate_str));
        px_index = ~cellfun(@isempty,strfind({subdirlist.name}, 'pos_px'));

        filename = fullfile(subdir, subdirlist(index2 & px_index).name);
        coords = dlmread(filename);

        maskPath = strrep(movieDataPath, 'movieData.mat', ...
            ['mask\fullImage_density_' strrep(num2str(density,'%.1f'),'.','-') '_'...
            num2str(replicate,'%02d'), '_mask.tif']);

        mask = imread(maskPath);

        ind1 = max(1,min(512,floor(coords(:,2))+1));
        ind2 = max(1,min(512,floor(coords(:,1))+1));
        filterIndex = logical(diag(mask(ind1,ind2)));

        x = coords(filterIndex,1);
        y = coords(filterIndex,2);
    else
        x = [];
        y = [];
    end


end