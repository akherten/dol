fittypeL = {'single', 'multi'};
thresholdL = [1.5, 2, 2.5];
% thresholdL = 1.5;

datasets_paths = {
    'y:\DOL Calibration\Data\SimData_realistic\green_background\Density-Simulation\analysis\thunderSTORM\points_allDensities_complete.mat'
    'y:\DOL Calibration\Data\SimData_realistic\red_background\Density-Simulation\analysis\thunderSTORM\points_allDensities_complete.mat'
};
channelsMask_dirs = {
    'y:\DOL Calibration\Data\SimData_realistic\green_background\Density-Simulation\3ChannelsMask\';
    'y:\DOL Calibration\Data\SimData_realistic\red_background\Density-Simulation\3ChannelsMask\';
};

for clr = [1,2]

    for loop1=1:length(fittypeL)
        for j=1:length(thresholdL)
            datasets_path = datasets_paths{clr};
            channelsMask_dir = channelsMask_dirs{clr};
            coords_root = 'y:\DOL Calibration\Data\SimData_realistic\points_raw_coords\';

            pixelsize = 104;

            fittype = fittypeL{loop1};
            threshold = num2str(thresholdL(j),'%.1f');

            simulations_pointExtraction_thunderSTORM(datasets_path, ...
                channelsMask_dir, pixelsize, fittype, threshold);

            simulations_cellAnalysis(0,[0.7 2.7],0.1:0.1:4)

            simulations_downstreamAnalysis;

            %simulations_plot

            % save workspace
            saveDir = strrep(datasets_paths{clr},'thunderSTORM\points_allDensities_complete.mat','');%'/home/klaus/Desktop/thunderSTORM_sims_parameterscreen_unfiltered/';
            savePath = strcat(saveDir,'thunderSTORM_',fittype,'_t-',strrep(num2str(threshold),'.','-'),'_fltr-0-0.7-2.7.mat');

            allV = who;
            nono = {'datasets','loop1','j','fittypeL','thresholdL','savePath','fName','datasets_paths','channelsMask_dirs'};
            saveme = setdiff(allV,nono);

            save(savePath,saveme{:});

            clear('datasets');

        end 
    end
    
end