matFilePaths = {
    'y:\DOL Calibration\Data\SimData_realistic\green_background\Density-Simulation\analysis\thunderSTORM_single_t-2-0_fltr-50-0.7-2.7.mat'
    'y:\DOL Calibration\Data\SimData_realistic\green_background\Density-Simulation\analysis\thunderSTORM_multi_t-2-0_fltr-50-0.7-2.7.mat'
    'y:\DOL Calibration\Data\SimData_realistic\green_background\Density-Simulation\analysis\u-track_green_single.mat'
    'y:\DOL Calibration\Data\SimData_realistic\green_background\Density-Simulation\analysis\u-track_green_multi.mat'
    'y:\DOL Calibration\Data\SimData_realistic\green_background\Density-Simulation\analysis\thunderSTORM_single_t-2-0.mat'
    'y:\DOL Calibration\Data\SimData_realistic\green_background\Density-Simulation\analysis\thunderSTORM_multi_t-2-0.mat'
};

varNames = {
    'ts1f'
    'tsxf'
    'uT1f'
    'uTxf'
    'ts1'
    'tsx'
};

for i = 1:length(matFilePaths)
    temp = load(matFilePaths{i}, 'data');
    eval([varNames{i} ' = temp.data']);
end

densities = uT1f.ID.GroundTruthDensity;

% Recall
% Errorbar plot of Recall in dependence of simulated point density
figure
axes
hold on
errorbar(densities, uT1f.Mean_Recall, uT1f.Std_Recall, 'x-','MarkerSize',10)
errorbar(densities, uTxf.Mean_Recall, uTxf.Std_Recall, 'x-','MarkerSize',10)
errorbar(densities, ts1.Mean_Recall, ts1.Std_Recall, 'x-','MarkerSize',10)
errorbar(densities, tsx.Mean_Recall, tsx.Std_Recall, 'x-','MarkerSize',10)
errorbar(densities, ts1f.Mean_Recall, ts1f.Std_Recall, 'x-','MarkerSize',10)
errorbar(densities, tsxf.Mean_Recall, tsxf.Std_Recall, 'x-','MarkerSize',10)
% unfiltered datasets
% errorbar(density, uT1.Mean_Recall, uT1.Std_Recall, 'x')
% errorbar(density, uTx.Mean_Recall, uTx.Std_Recall, 'x')
legend('u-track single', 'u-track multi', 'thunderSTORM single', 'thunderSTORM multi', 'ts single filtered', 'ts multi filtered')
xlabel('Ground Truth Density [um^-^2]')
ylabel('Recall')
xlim([0 2.7])
ylim([0 1])


% False Positives
% Errorbar plot of false positive density in dependence of simulated point
% density
figure
hold on
errorbar(densities, uT1f.Mean_DensityFalsePos, uT1f.Std_DensityFalsePos, 'x-','MarkerSize',10)
errorbar(densities, uTxf.Mean_DensityFalsePos, uTxf.Std_DensityFalsePos, 'x-','MarkerSize',10)
errorbar(densities, ts1.Mean_DensityFalsePos, ts1.Std_DensityFalsePos, 'x-','MarkerSize',10)
errorbar(densities, tsx.Mean_DensityFalsePos, tsx.Std_DensityFalsePos, 'x-','MarkerSize',10)
errorbar(densities, ts1f.Mean_DensityFalsePos, ts1f.Std_DensityFalsePos, 'x-','MarkerSize',10)
errorbar(densities, tsxf.Mean_DensityFalsePos, tsxf.Std_DensityFalsePos, 'x-','MarkerSize',10)
% unfiltered datasets
% errorbar(densities, uT1.Mean_DensityFalsePos, uT1.Std_DensityFalsePos)
% errorbar(densities, uTx.Mean_DensityFalsePos, uTx.Std_DensityFalsePos)
legend('u-track single', 'u-track multi', 'thunderSTORM single', 'thunderSTORM multi', 'ts single filtered', 'ts multi filtered')
xlabel('Ground Truth Density [um^-^2]')
ylabel('False Positive Density [um^-^2]')
xlim([-.1 2.7])
ylim([0 0.2])

return

% Point Filtering
% Histogram of amplitudes of background, low and high point density
% Amps = load('/media/smss_exchange/DOL Calibration/Data/SimData_realistic/red_background/analysis/u-track_single.mat', 'Points_Blue_A', 'GroundTruthDensity');
% unstained = [Amps.Points_Blue_A{Amps.GroundTruthDensity == 0}];
% highDens = [Amps.Points_Blue_A{Amps.GroundTruthDensity == 2.6}];
% lowDens = [Amps.Points_Blue_A{Amps.GroundTruthDensity == 0.6}];
% figure
% histogram(unstained, 'Normalization', 'count', 'BinWidth', 20);
% hold on
% histogram(lowDens, 'Normalization', 'count', 'BinWidth', 20);
% histogram(highDens, 'Normalization', 'count', 'BinWidth', 20);
% legend({'background only' '0.6 �m^-^2' '2.6 �m^-^2'})
% xlabel('Point Amplitude')
% ylabel('Count')
% xlim([0 2000])

% plot found vs. simulated density
figure
errorbar(uT1f.ID.GroundTruthDensity, uT1f.Mean_DensityBlue, uT1f.Std_DensityBlue, 'LineStyle', 'none')
xlim([-0.1 2.1])
ylim(xlim)
hold on
errorbar(uTxf.ID.GroundTruthDensity, uTxf.Mean_DensityBlue, uTxf.Std_DensityBlue, '+:','MarkerSize',3)
errorbar(ts1.ID.GroundTruthDensity, ts1.Mean_DensityBlue, ts1.Std_DensityBlue, '+:','MarkerSize',3)
errorbar(tsx.ID.GroundTruthDensity, tsx.Mean_DensityBlue, tsx.Std_DensityBlue, '+:','MarkerSize',3)
plot(xlim, ylim, '--k')
xlabel('simulated density [um^-^2]')
ylabel('found density [um^-^2]')
legend('u-track single', 'u-track multi', 'thunderSTORM single', 'thunderSTORM multi')

for i = 1:length(matFilePaths)
    densityCorrectionPlot(matFilePaths{i}, varNames{i});
end

function densityCorrectionPlot(matFilePath, varName)

    % Density Correction on u-track single emitter data
    load(matFilePath, 'DensityBlue', 'Recall', 'GroundTruthDensity');
    % fit doesn't accept recall = NaN for density = 0
    density = DensityBlue(~isnan(Recall));
    recall = Recall(~isnan(Recall));
    linfit = fit(density, recall, 'poly1');
%     conf = confint(linfit);
    figure
    scatter(DensityBlue, Recall)
    hold on
    plot(linfit)
    text(.1, .1, sprintf('Recall = %.4f * x + %.4f', linfit.p1, linfit.p2), 'Color', 'red')

    p11 = predint(linfit,xlim,0.95);
    plot(xlim,p11, 'r:');

    legend({varName 'linear fit' '95 % prediction'})
    xlabel('Found density [um^-^2]')
    ylabel('Recall')
    ylim([0 1])

end