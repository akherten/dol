function area = simulations_AreaFromMaskTS(movie,channelsMask_dir)

density = strrep(num2str(movie.concentration,'%01.1f'),'.','-');

if density == '0'
    density = '0-0';
end

maskFile = [channelsMask_dir,'density_',density,'/','fullImage_density_',density,'_',num2str(movie.replicate,'%02.f'),'_mask.tif'];

mask = imread(maskFile);
area = sum(mask(:) > 0);

end