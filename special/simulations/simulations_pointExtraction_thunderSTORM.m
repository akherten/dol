% Script to extract point data from thunderSTORM analysis of sims_realistic
% dataset.
%
% If input is not a movie-object data will be imported from input path.
% Either from mat-file (if existing mat-file path is provided) or from
% csv-files inside data folders. Import from .csv files is rather slow.

function simulations_pointExtraction_thunderSTORM(data, channelsMask_dir, pixelsize, fittype, threshold)

    switch class(data)
        case 'movie'
            fprintf('dataset already loaded. Proceeding.\n')
            datasets = data;
        case 'char'
            switch exist(data)
                case 2
                    fprintf('loading datasets .mat file.\n')
                    load(data);
                    datasets_path = data;
                case 7
                    fprintf('importing thunderSTORM data from .csv files.\n')
                    datasets = importTS(data);
                    data_dir = data;
            end
        otherwise
            error('input error');
    end
    clear data

    dsLength = length(datasets);

    % perform point filtering based on masks
    simulations_filterbyMasks_thunderSTORM(datasets,channelsMask_dir,pixelsize);

    % Assemble point arrays from datasets object

    [Points_Blue_x, Points_Blue_y, Points_Blue_A, Points_Blue_c, Points_Blue_s, ...
        ~, ~, ~, ~, ~, ~, ~, ~, ~, ~, ~, ~, replicate, ~] = convertTStoDOL(datasets,fittype,threshold);

    % Extract GTdensity and replicate from file list
    GroundTruthDensity = [datasets(:).concentration]';

    AllAreas = zeros(length(datasets),1);
    for i=1:length(datasets)
        AllAreas(i) = simulations_AreaFromMaskTS(datasets(i),channelsMask_dir);
    end

    clear('i','data_dir','datasets_path','channelsMask_dir','pixelsize','dsLength');

    % Load groundTruth data from file, extend by empty entries for density
    % = 0 and order according to data from point detection.
    
    GTdata = load('y:/DOL Calibration/Data/SimData_realistic/groundTruth.mat');

    Points_Green_x = cell(size(Points_Blue_x));
    Points_Green_y = cell(size(Points_Blue_y));

    for i = 1:length(GroundTruthDensity)
        idx = GTdata.GroundTruthDensity == GroundTruthDensity(i) & GTdata.replicate == replicate(i);
        switch sum(idx)
            case 0
                % density 0
                if GroundTruthDensity(i) == 0
                    Points_Green_x{i} = [];
                    Points_Green_y{i} = [];
                end
            case 1
                Points_Green_x(i) = GTdata.Points_Green_x(idx);
                Points_Green_y(i) = GTdata.Points_Green_y(idx);
            otherwise
                error('multiple files found')
        end
    end
    clear GTdata


    % assign variables in caller workspace
    vars = who;
    for n = 1:length(vars)
        assignin('caller',vars{n},eval(vars{n}))
    end

end
