% Script to collect point coordinates, identifiers and properties for the
% simulated cell data.

movieListPath = 'y:\DOL Calibration\Data\SimData_realistic\u-track\movieList_inclUnstained.mat';
savePath = '';
coords_root = 'y:\DOL Calibration\Data\SimData_realistic\raw';


movielist = load(movieListPath);
MDpaths = movielist.ML.movieDataFile_;

% assign cell properties derived from filename
[GroundTruthDensity, replicate] = simulations_conditionsFromString(MDpaths);

% collect points from u-track movieList
[PointDetectionParameters, Points_Blue_x, Points_Blue_y, Points_Blue_A,...
    Points_Blue_c, Points_Blue_s] = pointsFromMovieData(MDpaths, 1);    

% collect points from ground truth
[Points_Green_x, Points_Green_y] = ...
    simulations_pointsFromGroundTruth(GroundTruthDensity, replicate, coords_root, MDpaths);

% calculate cell areas from mask files
AllAreas = simulations_AreaFromMask(MDpaths);