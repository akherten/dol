function simulations_cellAnalysis(amplitudePercentile, sigmaRange, tolerance)

if ~nargin

    % Filtering amplitude threshold
    amplitudePercentile = [];  % set to [] to skip filtering

    % Filtering sigma range
    sigmaRange = []; % set to [] to skip filtering

    % Colocalisation tolerance    
    tolerance = (0.1:0.1:4);
    
end

Points_Blue_x = evalin('caller', 'Points_Blue_x');
Points_Blue_y = evalin('caller', 'Points_Blue_y');
Points_Blue_A = evalin('caller', 'Points_Blue_A');
Points_Blue_s = evalin('caller', 'Points_Blue_s');
Points_Blue_c = evalin('caller', 'Points_Blue_c');
Points_Green_x = evalin('caller', 'Points_Green_x');
Points_Green_y = evalin('caller', 'Points_Green_y');
GroundTruthDensity = evalin('caller', 'GroundTruthDensity');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Filter Points %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Keep unfiltered point amplitudes
Points_Blue_A_unfiltered = Points_Blue_A;
% Keep unfiltered point sigmas
Points_Blue_s_unfiltered = Points_Blue_s;
    

% Amplitude Filter
if ~isempty(amplitudePercentile)
                 
    % threshold from "unstained" cells (density = 0)
    reference = GroundTruthDensity == 0;
    % apply filtering on all cells
    experiment = true(length(GroundTruthDensity),1);

    [filterThreshold_Blue, Points_Blue_A, Points_Blue_x, Points_Blue_y, Points_Blue_c, Points_Blue_s] = ...
                    filterPointsByPercentile (amplitudePercentile, reference, experiment, ...
        Points_Blue_A, Points_Blue_x, Points_Blue_y, Points_Blue_c, Points_Blue_s);

end

% Sigma Filter
if ~isempty(sigmaRange)

    [Points_Blue_s, Points_Blue_A, Points_Blue_x, Points_Blue_y, Points_Blue_c] = ...
                    filterPointsByRange (sigmaRange, ...
        Points_Blue_s, Points_Blue_A, Points_Blue_x, Points_Blue_y, Points_Blue_c);

end



% Rotate Points %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Preallocate Variables
[Points_BlueRot_x, Points_BlueRot_y, Points_GreenRot_x, Points_GreenRot_y] = ...
    deal(cell(size(Points_Blue_x)));

for i = 1:length(Points_Blue_x)
    Points_BlueRot_x{i} = Points_Blue_y{i};
    Points_BlueRot_y{i} = 512 - Points_Blue_x{i};
    Points_GreenRot_x{i} = Points_Green_y{i};
    Points_GreenRot_y{i} = 512 - Points_Green_x{i};
end


% Colocalisation %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('Colocalisation analysis')

% Preallocate Variables
[BlueParticles, GreenParticles]...
    = deal(zeros(length(Points_Blue_x),1));

[ColocalizationBlueGreen, ColocalizationGreenRandom, pGreen, pBlue,...
    pBlue2, pGreenRandom, multipleassigned_particlesGreen,...
    ColocalizationGreenRandom, multipleassigned_particlesGreenRandom]...
    = deal(zeros(length(Points_Blue_x),length(tolerance)));
    
for t = 1:length(tolerance)
    dispProgress(t, length(tolerance))
    
    % parallelized detection of colocalisation, temporal assignment to xxxT
    % variables
    [ColocalizationBlueGreenT, multipleassigned_particlesGreenT, ...
        ColocalizationGreenRandomT, multipleassigned_particlesGreenRandomT]...
        = deal(zeros(length(Points_Blue_x),1));
    toleranceT = tolerance(t);
    
    parfor i = 1:length(Points_Blue_x)
        [BlueParticles(i), GreenParticles(i), ColocalizationBlueGreenT(i), multipleassigned_particlesGreenT(i)] = ...
            detectColocalisation(Points_Blue_x{i}, Points_Blue_y{i}, ...
            Points_Green_x{i}, Points_Green_y{i}, ...
            toleranceT, toleranceT);

        [~, ~, ColocalizationGreenRandomT(i),multipleassigned_particlesGreenRandomT(i)] = ...
            detectColocalisation(Points_Blue_x{i}, Points_Blue_y{i}, ...
            Points_GreenRot_x{i}, Points_GreenRot_y{i}, ...
            toleranceT, toleranceT);
    end
    
    ColocalizationBlueGreen(:,t) = ColocalizationBlueGreenT;
    multipleassigned_particlesGreen(:,t) = multipleassigned_particlesGreenT;
    ColocalizationGreenRandom(:,t) = ColocalizationGreenRandomT;
    multipleassigned_particlesGreenRandom(:,t) = multipleassigned_particlesGreenRandomT;
    pBlue(:,t) = ColocalizationBlueGreenT ./ GreenParticles;
    pGreen(:,t) = ColocalizationBlueGreenT ./ BlueParticles;
    pGreenRandom(:,t) = ColocalizationGreenRandomT ./ BlueParticles;
end


vars = who;
for n = 1:length(vars)
    assignin('caller',vars{n},eval(vars{n}))
end

end