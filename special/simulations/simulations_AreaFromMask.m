function Area = simulations_AreaFromMask(MDpaths)

    if iscellstr(MDpaths)
        Area = zeros(length(MDpaths),1);
        for i = 1:length(MDpaths)
            dispProgress(i, length(MDpaths))
            [Area(i)] = getArea(MDpaths{i});
        end
    elseif ischar(MDpaths)
        [Area] = getArea(MDpaths);
    else
        error('Input Error. movieDataPath must be string or cell array of strings')
    end

end

function Area = getArea(MDpath)

    
    maskPath = strrep(MDpath, 'movieData.mat', 'mask');
    dirlist = dir(maskPath);
    maskFile = fullfile(dirlist(3).folder, dirlist(3).name);
    
    mask = imread(maskFile);
    Area = sum(mask(:) > 0);

end