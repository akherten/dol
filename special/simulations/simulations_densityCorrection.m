
% Density Correction on u-track single emitter data

% load 'DensityBlue', 'Recall', 'GroundTruthDensity'
load('y:\DOL Calibration\Data\SimData_realistic\green_background\Density-Simulation\analysis\u-track_green_single.mat', 'DensityBlue', 'Recall', 'GroundTruthDensity');
% fit doesn't accept recall = NaN for density = 0
density = DensityBlue(~isnan(Recall));
recall = Recall(~isnan(Recall));
linfit = fit(density, recall, 'poly1');
conf = confint(linfit);
figure
scatter(DensityBlue, Recall)
hold on
plot(linfit)
text(.1, .1, sprintf('Recall = %.4f * x + %.4f', linfit.p1, linfit.p2), 'Color', 'red')

p11 = predint(linfit,xlim,0.95);
plot(xlim,p11, 'r:');

xlabel('Found density [um^-^2]')
ylabel('Recall')
ylim([0 1])

