ml = {
'y:\DOL Calibration\Data\SimData_realistic\green_background\Density-Simulation\u-track single\movieList_Density-Simulation_green_single.mat'
'y:\DOL Calibration\Data\SimData_realistic\green_background\Density-Simulation\u-track multi\movieList_Density-Simulation_green_multi.mat'
'y:\DOL Calibration\Data\SimData_realistic\red_background\Density-Simulation\u-track single\movieList_Density-Simulation_red_single.mat'
'y:\DOL Calibration\Data\SimData_realistic\red_background\Density-Simulation\u-track multi\movieList_Density-Simulation_red_multi.mat'
};

coords_root = 'y:\DOL Calibration\Data\SimData_realistic\points_raw_coords\';

sp = {
'y:\DOL Calibration\Data\SimData_realistic\green_background\Density-Simulation\analysis\u-track_green_single_fltr-0-0.7-2.7.mat'
'y:\DOL Calibration\Data\SimData_realistic\green_background\Density-Simulation\analysis\u-track_green_multi_fltr-0-0.7-2.7.mat'
'y:\DOL Calibration\Data\SimData_realistic\red_background\Density-Simulation\analysis\u-track_red_single_fltr-0-0.7-2.7.mat'
'y:\DOL Calibration\Data\SimData_realistic\red_background\Density-Simulation\analysis\u-track_red_multi_fltr-0-0.7-2.7.mat'
};

for i = 1:4
    pipeline(ml{i},coords_root,sp{i});
end

function pipeline(movieListPath, coords_root, savePath)

simulations_pointExtraction

simulations_cellAnalysis([],[0.7 2.7],(0.1:0.1:4));

simulations_downstreamAnalysis

save(savePath);

end

% simulations_plot